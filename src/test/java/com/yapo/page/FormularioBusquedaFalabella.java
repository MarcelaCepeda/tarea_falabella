package com.yapo.page;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.internal.WebElementToJsonConverter;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.yapo.config.BaseConfig;

public class FormularioBusquedaFalabella extends BaseConfig{
	
	public FormularioBusquedaFalabella(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	

	@FindBy(xpath = "/html/body/div[1]/nav/div[3]/div/div[1]/div/div[1]/div/input")
	public WebElement inputBusqueda;
	
	@FindBy(xpath = "//li[@data-undefined-price][contains(@class,'price-0')]")
	public List <WebElement> arreglo;
	
	@FindBy(xpath = "//*[@id=\"__next\"]/div/section/div[1]/div[1]/div/div/div/div[2]/section[2]/div/div/div/div[4]/div/div[2]/div[1]")
	public WebElement btnTalla;
	
	@FindBy(xpath = "//*[@id=\"buttonForCustomers\"]/button")
	public WebElement btnAnadir;
	
	
	@FindBy(xpath = "//*[@id=\"linkButton\"]")
	public WebElement btnVerCarro;

	@FindBy(xpath = "//*[@id=\"root\"]/div[2]/div[2]/div/form/div[2]/div[2]/div[2]/button")
	public WebElement btnIrComprar;

	@FindBy(xpath = "//*[@id=\"region\"]")
	public WebElement selectRegion;
	
	@FindBy(xpath = "//*[@id=\"comuna\"]")
	public WebElement selectComuna;
	
	@FindBy(xpath = "//*[@id=\"fbra_checkoutRegionAndComuna\"]/div/section/section/form/div/div[3]/div/button")
	public WebElement btnContinuar;
	
	@FindBy(xpath = "//*[@id=\"calle\"]")
	public WebElement txtCalle;
	
	@FindBy(xpath = "//*[@id=\"streetNumber\"]")
	public WebElement txtNumero;
	
	@FindBy(xpath = "//*[@id=\"fbra_checkoutDeliverAndCollect\"]/div/div/div[1]/div/div[2]/div/div/form/section/div/div[5]/button")
	public WebElement btnAgregarDire;
	
	@FindBy(xpath = "//*[@id=\"fbra_checkoutDeliveryActions\"]/div/div/span/div/div/div/div[2]/button")
	public WebElement btnContinuar2;
	
	
	
	
	
	/*----------------------------------------------------------*/
	
	public boolean BusquedaInput (String Busqueda) {
		try {

			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.elementToBeClickable(inputBusqueda)).click();
			inputBusqueda.sendKeys(Busqueda);
			inputBusqueda.sendKeys(Keys.ENTER);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	

	
	public void buscarCerc(int valorbuscar) {
		
		List<Integer> list = new ArrayList<Integer>();
		for (WebElement elemento: arreglo) {
			String cosa = elemento.getAttribute("data-undefined-price").trim().replace(".","");
			Integer elementList = Integer.parseInt(cosa);
			list.add(elementList);
			
		}
		Integer[] array = list.toArray(new Integer[0]);
		int cercano = masCercano(array, 10000);
		System.out.println(cercano);
		
		
		DecimalFormat formato = new DecimalFormat("###,###");
		
		String strCercano = String.valueOf(formato.format(cercano));
		System.out.println(strCercano);
		
		WebElement selectProducto = driver.findElement(By.xpath("//li[@data-undefined-price=\""+strCercano+"\"][contains(@class,'price-0')]"));
		selectProducto.click();
		
		
	}
	
	public static Integer masCercano(Integer[] numeros, Integer num) {
		
        int cercano = 0;
        int diferencia = Integer.MAX_VALUE; //inicializado valor máximo de variable de tipo int
        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] == num) {
                return numeros[i];
            } else {
                if(Math.abs(numeros[i]-num)<diferencia){
                    cercano=numeros[i];
                    diferencia = Math.abs(numeros[i]-num);
                }
            }
        }
        return cercano;
    }
	
	public void pressAnadir() {
		btnAnadir.click();
	}
	
	public boolean pressTalla() {
		try {

			btnTalla.click();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}
	
	public void pressVerCarro() {
		btnVerCarro.click();
	}
	
	public void pressIrComprar() {
		btnIrComprar.click();
	}
	
	public boolean Region (String Region) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 3);
			wait.until(ExpectedConditions.elementToBeClickable(selectRegion));
			Select selectRegionn = new Select(selectRegion);
			selectRegionn.selectByVisibleText(Region);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean Comuna (String Comuna) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 3);
			wait.until(ExpectedConditions.elementToBeClickable(selectComuna));
			Select selectComunaa = new Select(selectComuna);
			selectComunaa.selectByVisibleText(Comuna);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public void pressContinuar() {
		btnContinuar.click();
	}
	
	public boolean Calle (String calle) {
		try {
			
			txtCalle.sendKeys(calle);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean Numero (String numero) {
		try {
			
			txtNumero.sendKeys(numero);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public void pressAgregar() {
		btnAgregarDire.click();
	}
	
	public void pressContinuar2() {
		btnContinuar2.click();
	}
	
	
}	

	
	
	
	
	