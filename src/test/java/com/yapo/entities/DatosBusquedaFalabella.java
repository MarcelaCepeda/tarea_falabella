package com.yapo.entities;

public class DatosBusquedaFalabella {

	String  Busqueda;
	String  Region;
	String  Comuna;
	String  calle;
	String  numero;
	
	public String getBusqueda() {
		return Busqueda;
	}
	public void setBusqueda(String busqueda) {
		Busqueda = busqueda;
	}
	public String getRegion() {
		return Region;
	}
	public void setRegion(String region) {
		Region = region;
	}
	public String getComuna() {
		return Comuna;
	}
	public void setComuna(String comuna) {
		Comuna = comuna;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
}
