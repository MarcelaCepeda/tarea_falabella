package com.yapo.test;


import org.testng.asserts.SoftAssert;
import org.testng.annotations.Test;
import com.yapo.config.BaseConfig;
import com.yapo.dataprovider.DataProviderYapito;
import com.yapo.page.FormularioBusquedaFalabella;
import com.yapo.entities.DatosBusquedaFalabella;


public class BusquedaFalabella extends BaseConfig {
	@Test(description="Busqueda en Falabella", dataProvider = "buscarFalabella", dataProviderClass = DataProviderYapito.class)
	public void busquedaFalabella(DatosBusquedaFalabella datos) throws InterruptedException {
		
		SoftAssert soft = new SoftAssert();	
		FormularioBusquedaFalabella formulario = new FormularioBusquedaFalabella(driver);
		
		//Thread.sleep(10000);
		soft.assertTrue(formulario.BusquedaInput(datos.getBusqueda()),"se cayo no encontro input busqueda");
		formulario.buscarCerc(10000);
		soft.assertTrue(formulario.pressTalla(),"se cayo en talla");
		//formulario.pressTalla();
		formulario.pressAnadir();
		formulario.pressVerCarro();
		formulario.pressIrComprar();
		soft.assertTrue(formulario.Region(datos.getRegion()));
		soft.assertTrue(formulario.Comuna(datos.getComuna()));
		formulario.pressContinuar();
		soft.assertTrue(formulario.Calle(datos.getCalle()));
		soft.assertTrue(formulario.Numero(datos.getNumero()));
		formulario.pressAgregar();
		formulario.pressContinuar2();
		soft.assertAll();

	}
}

